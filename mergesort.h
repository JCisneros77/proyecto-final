#ifndef mergesort
#define mergesort

/**Procedimiento que mezcla dos arreglos ordenados
 * @param *izq primer arreglo de la mezcla
 * @param *der segundo arreglo de la mezcla
 * @param tamanoizq numero de elementos del primer arreglo de la mezcla
 * @param tamanoder numero de elementos del segundo arreglo de la mezcla
 * @return arreglo resultante de la mezcla de los dos arreglos
 */
int *mezclar (int *izq , int *der, int tamanoizq, int tamanoder);

#endif
