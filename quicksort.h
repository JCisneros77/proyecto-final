#ifndef quicksort
#define quicksort

/**Procedimiento que implementa quicksort
 * @param *array arreglo a ordenar
 * @param tamano tamano del arreglo a ordenar
 */
void quickSort (int *array, int tamano);

/**Llamada recursiva que realiza el procedimiento quicksort
 * @param array arreglo a ordenar
 * @param izq limite izquierdo de la seccion de arreglo a ordenar
 * @param der limite derecho de la seccion de arreglo a ordenar
 */
void q_sort(int *array, int izq, int der);

#endif
