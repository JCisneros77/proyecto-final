#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include "quicksort.h"
#include "mergesort.h"

/**
* Lee números dados por el usuario mediante
* la entrada estándar. Posteriormente los guarda en
* un archivo binario.
* @param tamano es la cantidad de numeros a ordenar
*/

int *lecturaInicial (int tamano) {
     int i;
     int num;
    printf("*-- Nota\n Utilizar números entre 0 y 2,147,483,647 --* \n");
    char buffer[16];
    int *res = (int*)malloc(tamano*sizeof(int));
    FILE *f1;
    f1 = fopen ("archivoConNumeros.bin", "wb");
   	for (i = 0; i < tamano; ++i){
		printf("Escribe un número: \n");
		if (fgets(buffer, 16, stdin) != NULL) {
		num = atoi(buffer);
	        res[i] = num;
		}	
	}
	fwrite(res, sizeof(int), tamano, f1);
	fclose(f1);

    return res;
  
}

/**
* Lee un arreglo de enteros proveniente de un 
* archivo binario
* @param archivo[] nombre del archivo de texto donde
* se encuentran los numeros
* @param tamano cantidad de numeros del archivo
* @return Arreglo de enteros del archivo
*/

int *lectura (char archivo[], int tamano) {
 
    int *res = (int*)malloc(tamano*sizeof(int));
    FILE *f1;
    f1 = fopen (archivo, "rb");
    fread(&res[0], sizeof(int), tamano, f1);
    return res;
  
}

/**
 * Escribe un arreglo en un archivo binario
 * @param archivo[] nombre del archivo de texto donde se
 * escribiran los numeros
 * @param tamano cantidad de elementos del arreglo
 * @param *res arreglo a escribir en el archivo
 */
void escritura (char archivo[], int tamano, int *res) {
    FILE *f1;
    f1 = fopen (archivo, "w");
    fwrite(&res[0], sizeof(int), tamano, f1);

}

/**
 * Procedimiento que dado un arreglo proveniente de un archivo de texto,
 * lo ordena usando un arbol de procesos, en el cual usa quicksort en sus
 * hojas y el algoritmo de mezcla en los niveles intermedios
 * @param niveles numero de niveles del arbol de procesos
 * @param archivo[] nombre del archivo de texto donde se encuentra el 
 * arreglo a ordenar 
 * @param tamano numero de elementos del arreglo a ordenar
 * @param salida nombre del archivo en el que se escribira el arreglo ordenado
 * en formato texto
 */
void arbol_procesos(int niveles, int tamano, char salida[]) {
  
  //Inicializacion de datos
  char archivo[] = "archivoConNumeros.bin";
  int *res = lecturaInicial(tamano);
  int *arr1, *arr2, *mezcla;
  pid_t hijoizq;
  pid_t hijoder;
  int i,k;
  int ni, inicio;
  char nombre[50], izq[50], der[50];
  long pid;
  ni =tamano;
  inicio=0;
  char niString[50],tamString[50],iniString[50];
  
  //Creacion de los procesos hijos
  for(i=0;i<niveles;i++) {
    //Crea el hijo izquierdo
    hijoizq = fork();
  
    //Si es el hijo izquierdo
    if (hijoizq == 0) {
      
      ni = ni/2;
      
      //Si es hoja del arbol
      if (i==(niveles-1)){
	 sprintf(niString, "%d", ni);
	 sprintf(iniString, "%d", inicio);
	 sprintf(tamString, "%d", tamano);
	 execl("hoja","hoja",niString,iniString,archivo,tamString,NULL);
      }
      continue;
      
    }
    
    //Si no es el proceso hijo
    else {
      
      //Crea el hijo derecho
      hijoder = fork();
      
      //Si es el hijo derecho
      if (hijoder == 0 ) {
	 inicio = inicio + ni/2;
         ni = ni - ni/2;
	
	 //Si es hoja del arbol
	 if (i==(niveles-1)){
	  sprintf(niString, "%d", ni);
	  sprintf(iniString, "%d", inicio);
	  sprintf(tamString, "%d", tamano);
	  execl("hoja","hoja",niString,iniString,archivo,tamString,NULL);
	 }
      continue;
      }
      
      //EL proceso principal espera por sus procesos hijos
      wait(0);
      wait(0);
      
      //Leer los archivos provenientes de los hijos y mezclar
      sprintf(izq, "%ld", (long)hijoizq);
      sprintf(der, "%ld", (long)hijoder);
      strcat(izq,".txt");
      strcat(der,".txt");
      arr1=lectura(izq,ni/2);
      arr2=lectura(der,ni-ni/2);
      mezcla=mezclar(arr1,arr2,ni/2,ni-ni/2);
      remove(izq);
      remove(der);
      free(arr1);
      free(arr2);
      sprintf(nombre, "%ld", (long)getpid());
      strcat(nombre,".txt");
      escritura(nombre,ni,mezcla);
      if (i==0) {
	  remove(nombre);
	  FILE *f2;
	  f2 = fopen (salida, "w");
	  for (k=0; k<tamano; k++) {
	    fprintf(f2,"%d\n",mezcla[k]);
	    
	  }
	  free(mezcla);
      }
      break;
    }
  }
}
 

/**Procedicimiento principal que se realiza en el 
 * ejecutable 
 * @param argc numero de argumentos que entran al procedimiento
 * @param argv argumentos que entran por consola estos son 
 * argv[1] numero de elementos a ordenar
 * argv[2] niveles del arbol de procesos
 */ 
int main(int argc, char *argv[]) {
  
  arbol_procesos(atoi(argv[2]),atoi(argv[1]),"salida.txt");
  
}
