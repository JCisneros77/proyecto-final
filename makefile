all: ordenArchivo-p

ordenArchivo-p: quicksort.o mergesort.o  ordenArchivo-p.o 
	gcc quicksort.o mergesort.o  ordenArchivo-p.o -o ordenArchivo-p

quicksort.o: quicksort.c
	 gcc -c quicksort.c
	 
mergesort.o: mergesort.c
	 gcc -c mergesort.c

ordenArchivo-p.o: ordenArchivo-p.c
	gcc -c ordenArchivo-p.c
	
all: hoja 

hoja: quicksort.o hoja.o
	gcc hoja.c -o hoja

clean: 
	rm quicksort.o hoja.o mergesort.o ordenArchivo-p.o ordenArchivo-p hoja salida.txt archivoConNumeros.bin

	
