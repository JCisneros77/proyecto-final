#include <stdio.h>
#include <stdlib.h>

/**Procedimiento que mezcla dos arreglos ordenados
 * @param *izq primer arreglo de la mezcla
 * @param *der segundo arreglo de la mezcla
 * @param tamanoizq numero de elementos del primer arreglo de la mezcla
 * @param tamanoder numero de elementos del segundo arreglo de la mezcla
 * @return arreglo resultante de la mezcla de los dos arreglos
 */
int *mezclar (int *izq , int *der, int tamanoizq, int tamanoder) {
  
  int *res = (int *)malloc(tamanoizq*sizeof(int)+tamanoder*sizeof(int));
  int i,j,k;
  i=0;
  j=0;
  k=0;
  
  while ((i<tamanoizq)&&(j<tamanoder)){
    
    if (izq[i]<der[j]) {
      res[k]=izq[i];
      i++;
      k++;
    }
    if (izq[i]>=der[j]) {
      res[k]=der[j];
      j++;
      k++;
    }
  }
  
  if (i==tamanoizq) {
    while (j<tamanoder) {
      res[k]=der[j];
      k++;
      j++;
    }
  }
  
  if (j==tamanoder) {
    while (i<tamanoizq) {
      res[k]=izq[i];
      k++;
      i++;
    }
  }
  return res;
  
}
