#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include "quicksort.c"

/** Funcion ejecutable, que ordena secciones del arreglo 
 *  principal con la funcion quicksort
 *  @param argc numero de argumentos que le entran al procedimiento
 *  @param argv argumentos que le entran al procedimiento
 *  argv[1] numero de elementos de la seccion a arreglar
 *  argv[2] posicion de inicio del arreglo original
 *  argv[3] archivo de texto con el arreglo principal
 *  argv[4] Tamano del arreglo principal
 */
void main(int argc, char *argv[]) {
    int inicio = atoi(argv[2]);
    int ni = atoi(argv[1]);
    int tamano = atoi(argv[4]);
    int *res = (int*)malloc(tamano*sizeof(int));
    FILE *f1;
    f1 = fopen (argv[3], "rb");
    fread(&res[0], sizeof(int), tamano, f1);
    int *ord = (int*)malloc(ni*sizeof(int));
    int k;
    long pid;
    char nombre[50];
    for (k = 0; k <ni ; k++) {
      ord[k]=res[inicio+k];
    }
    quickSort(ord,ni);
    pid = (long)getpid();
    sprintf(nombre, "%ld", pid);
    strcat(nombre,".txt");
    FILE *f2;
    f2 = fopen (nombre, "w");
    fwrite(&ord[0], sizeof(int), ni, f2);
    free(ord);
} 
