#include "quicksort.h"

/**Procedimiento que implementa quicksort
 * @param *array arreglo a ordenar
 * @param tamano tamano del arreglo a ordenar
 */
void quickSort (int *array, int tamano) {
  
  q_sort(array,0,tamano-1);
  
}

/**Llamada recursiva que realiza el procedimiento quicksort
 * @param array arreglo a ordenar
 * @param izq limite izquierdo de la seccion de arreglo a ordenar
 * @param der limite derecho de la seccion de arreglo a ordenar
 */
void q_sort(int *array, int izq, int der) {
  
  int pivote,l_hold,r_hold;
  
  l_hold=izq;
  r_hold=der;
  pivote=array[izq];
  
  while (izq<der){
  
    while ((array[der]>=pivote)&&(izq<der)) {
      der--;
    }
    if (izq !=der){
      array[izq]=array[der];
      izq++;
    }
    while ((array[izq]<=pivote)&&(izq<der)) {
      izq++;
    }
    if (izq != der) {
      array[der]=array[izq];
      der--;
    }
  }
  array[izq]=pivote;
  pivote=izq;
  izq=l_hold;
  der=r_hold;
  if (izq < pivote ) {
    q_sort (array,izq,pivote-1);
  }
  if (der > pivote ) {
    q_sort(array,pivote+1,der);
  }
  
}
